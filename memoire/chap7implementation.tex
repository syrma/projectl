\chapter{Implémentation}
Dans ce chapitre, nous allons aborder les détails techniques de la réalisation de notre projet. Aux deux chapitres précédents, nous avons commencé par identifier les entités de notre système, les relations qui existent entre elles, puis plus spécifiquement dans le chapitre précédent, leur façon d'interagir afin de constituer un système pouvant passer à la phase d'implémentation. Ce passage se fera à partir des résultats des deux phases d'analyse et de conception.

C'est ce que nous aborderons en théorie dans la suite de ce chapitre, mais également en pratique, en illustrant des choix purement techniques que nous avons faits. Nous commencerons par présenter les divers outils dont nous nous sommes servis pour l'implémentation, puis nous expliquerons le passage depuis un modèle statique d'analyse vers un modèle relationnel. Enfin, nous terminerons par présenter quelques interfaces des fonctionnalités de l'application.


\section{Modèles d'architecture}

\subsection{Architecture Modèle-Vue-Contrôleur}

Modèle-vue-contrôleur, ou MVC, est une architecture de conception logicielle pour l'implémentation des interfaces utilisateur. Elle divise l'application d'un logiciel donné en trois parties liées, de façon à séparer les représentations internes des informations de la façon dont elles sont présentées à l'utilisateur.
Comme pour d'autres modèles d'architecture logicielles, MVC exprime le principe de la solution à un problème, tout en lui permettant de s'adapter à chaque système. Les architectures MVC dans des cas particuliers peuvent différer significativement de la description théorique du modèle.

Généralement toutefois, MVC est divisé en trois composants dont il tient les noms :

\begin{description}
\item[Le modèle] composant central d'MVC, contient le comportement de l'application, en termes de ses fonctionnalités en vue desquelles elle a été développée, indépendamment de l'interface utilisateur. Le modèle gère directement les données, la logique et les règles de l'application.  
\item[La vue] qui peut être n'importe quel représentation de l'information en sortie ; une information peut également avoir plusieurs représentations.
\item[Le contrôleur] qui reçoit des requêtes et les interprète en instructions pour la vue ou pour le modèle.
\end{description}

\subsection{Data Access Object}

Cette approche consiste à utiliser un objet, nommé \emph{Data Access Object} (DAO), qui offre une interface abstraite à une base de données ou à un système de stockage quelconque. Concrètement, cette approche vise à séparer l'accès à la base de données des classes métier. Ainsi, les contrôleurs feront appel aux objets DAO pour tout accès aux données, rendant la méthode de stockage de ces dernières totalement masquée.

\begin{description}
\item[Avantages] :
  \mbox{}
  \begin{itemize}
  \item Séparation relativement simple et rigoureuse de deux parties de l'applications censées être indépendantes l'une de l'autre.
  \item Possibilité de changement de méthode de stockage (ou de SGBDR) sans devoir réimplémenter la couche métier.
  \end{itemize}
\item[Inconvénients]
  \mbox{}
  \begin{itemize}
  \item Beaucoup de code répétitif.
   \item Possibilité d'abstraire des fonctionnalités dont l'utilisateur de ces objets peut avoir besoin, le contraignant à leur réimplémentation (et augmentant ainsi le risque d'erreurs).
  \end{itemize}
\end{description}

\section{Ressources matérielles}
\begin{table}[H]
\begin{center}
\begin{tabular}{|l|ll|}
  \hline
  Type d'ordinateur & Configuration & \\
  \hline
   Ordinateur de bureau & Mémoire & 4 Go  \\
  & Processeur & Intel(R) Core(TM) i5-2300 CPU @ 2.80Ghz x 4 \\
  & Carte graphique & Intel(R) Sandybridge Desktop x89/MMX/SSE2 \\
  & Système d'exploitation & Debian GNU/Linux 8 (jessie) 32 bits \\
  & Disque dur & 485,0 Go \\
  \hline
  Ordinateur portable & Mémoire & 4 Go \\
  & Processeur & Intel(R) Core(TM) i7 CPU @ 2.30GHz x 6 \\
  & Carte graphique & AMD Readon HD 7670M \\
  & Système d'exploitation & Windows 7 Édition familliale Premium 64 bits \\
  & Disque dur & 600,0 Go \\
  \hline
  Ordinateur portable & Mémoire & 6,0 Go \\
  & Processeur &  Intel(R) Core(TM) i5-2430 CPU @ 2.40Ghz x 4 \\
  & Carte graphique & Intel(R)HD Graphics 3000 \\
  & Système d'exploitation & Windows 7 Édition Intégrale 64 bits \\
  & Disque dur & 750,0 Go \\
  \hline
\end{tabular}
\end{center}
\caption{Table résumant les ressources matérielles utilisées lors du projet.}
\end{table}
  

\section{Outils de développement}
\subsection{Java Entreprise Edition}

\emph{Java Enterprise Edition}\cite{java}, abrégé en Java EE (anciennement J2EE), est une spécification pour la technique Java d'Oracle plus particulièrement destinée aux applications d'entreprise. Ces applications sont considérées dans une approche multi-niveaux. Dans ce but, toute implémentation de cette spécification contient un ensemble d'extensions au \emph{framework} Java standard (JSE, \emph{Java Standard Edition}) afin de faciliter notamment la création d'applications réparties.

Pour ce faire, Java EE définit les éléments suivants :
\begin{itemize}
\item Une plate-forme (\emph{Java EE Platform}), pour héberger et exécuter les applications, incluant outre Java SE des bibliothèques logicielles additionnelles du \emph{Java Developmen Kit} (JDK) ;
\item Une suite de tests (\emph{Java EE Compatibility Test Suite}) pour vérifier la compatibilité ;
\item Une réalisation de référence (\emph{Java EE Reference Implementation}), dénommée GlassFish ;
\item Un catalogue de bonnes pratiques (\emph{Java EE BluePrints}).
\end{itemize}

Java EE complète le \emph{framework} standard par un ensemble de bibliothèques logicielles adaptées à des applications professionnelles. Les applications réalisées grâce à cette édition peuvent s'exécuter sur le même JRE (\emph{Java Runtime Environment}) qu'une application écrite avec Java SE. Un << conteneur Java >> lourd sera cependant nécessaire.

Pour notre application, nous utiliserons le conteneur open-source Apache Tomcat.

\subsection{Apache Tomcat}

\emph{Apache Tomcat}\cite{tomcat} est un serveur web et un conteneur de servlet libre développé par l'\emph{Apache Software Foundation}. Il implémente plusieurs spécifications de Java EE dont \emph{Java Servlet}, \emph{JavaServer Pages} (JSP), \emph{Java EL}, et \emph{WebSocket}, et offre un serveur HTTP en java pur, permettant à du code Java de s'y exécuter.

Tomcat inclue les composants suivants depuis sa version 4.0 :
\begin{description}
\item[Catalina] le conteneur de servlet de Tomcat. Il implémente les spécifications de \emph{Sun Microsystems} des servlet, ainsi que les JSP. 
\item[Coyote] le connecteur HTTP de Tomcat. Il gère en tant que serveur web le protocole HTTP 1.1. Ceci permet à Catalina, un conteneur pour Servlets et JSP, d'agir en tant que serveur web pour les fichiers locaux en tant que documents HTTP. Coyote écoute sur un port TCP donné, et envoie les requêtes reçues au moteur Tomcat pour traiter la requête et retourner une réponse au client. 
\item[Jasper] le moteur JSP de Tomcat. C'est le composant chargé de compiler les JSP et les transformer en servlets (qui seront ensuite gérés par Catalina). Jasper 2, utilisé par Tomcat depuis sa version 5, inclue plusieurs fonctionnalités supplémentaires dont celle de détecter les changements dans les fichiers JSP et recompiler les fichiers modifiés lors de son exécution.
\item[Cluster] un composant gérant les grandes applications. Il est utilisé pour réaliser la répartition de charge selon différentes méthodes, et requiert la version 1.5 ou une version ultérieure de JDK pour fonctionner. 
\end{description}

Pour le cas de notre application, nous utiliserons \emph{Apache Tomcat 7}. Nous pouvons, en conclusion, dire que c'est ce conteneur qui nous permettra d'exécuter notre application développée à l'aide de Java EE puisqu'il gère, en plus des éléments java standards, les servlets et les pages JSP indispensables à notre architecture. Toutefois, pour compléter cette dernière, nous aurons également besoin d'un système de stockage de données, chose qui n'est pas offerte par les composants précédemment cités. En l'occurrence, nous aurons besoin d'un système de gestion de bases de données relationnel.

\subsection{MySQL}
MySQL\cite{mysql} est un système de gestion de bases de données relationnelles (SGBDR), le deuxième SGBDR plus utilisé au monde et premier dans la catégorie des SGBDR libres. Il est distribué sous une double licence ; son code source est mis à la disponibilité du public selon les termes de la licence GPL (\emph{General Public Licence}) de GNU, mais le logiciel est également distribué sous une variété de contrats propriétaires. Il est actuellement la propriété de \emph{Oracle Corporation}.

MySQL n'est accompagné d'aucune interface utilisateur graphique (GUI) permettant d'administrer les bases de données MySQL ou de gérer les données contenues. L'utilisateur peut utiliser l'outil de ligne de commande, ou la variété de logiciels et d'applications web qui proposent de créer et de gérer les bases de données, ainsi que d'autres fonctionnalités. Un outil gratuit et officiel, développé activement par Oracle, est MySQL Workbench.

MySQL a été développé dans un souci de performances élevées en lecture, ce qui signifie qu'il est davantage orienté vers le service de données déjà en place que vers celui de mises à jour fréquentes et fortement sécurisées. Il est multi-thread et multi-utilisateur. Il supporte supporte deux langages informatiques, le langage de requête SQL et le SQL/PSM (Persistent Stored Modules), une extension procédurale standardisée au SQL incluse dans la norme SQL:20035. Il fonctionne sous de nombreux systèmes d'exploitation différents, d'où sa vaste utilisation.

Les bases de données sont généralement accessibles en utilisant un grand nombre de langages de programmation, dont Java. Une \emph{API} est disponible pour l'exploitation des bases de données depuis chaque langage ; dans le cas de Java, celle qu'on utilisera est JDBC.


\subsection{JDBC}

JDBC (\emph{Java Database Connectivity})\cite{java} est une technologie de la plate-forme \emph{Java Standard Edition} pour la connexion aux bases de données. Il s'agit d'une \emph{API} Java qui définit la façon dont un client accède à une base de données, et offre des méthodes pour exécuter des requêtes et y modifier des données. JDBC est orienté vers les bases de données relationnelles, et gère les fonctionnalités de base du langage SQL. Elle fait partie intégrante de Java SE depuis la version 1.1 de JDK.

Son principal atout est qu'elle permet aux applications Java d'accéder par le biais d'une interface commune à des sources de données pour lesquelles il existe des pilotes JDBC. Ses fonctionnalités sont donc indépendantes du SGBD utilisé.

Les classes du JDBC sont inclues dans les packages java.sql et javax.sql. JDBC permet à plusieurs de ses implémentations de coexister et d'être utilisées par une même application. L'\emph{API} offre un méchanisme pour charger dynamiquement les bons packages Java et de les enregistrer avec le JDBC Driver Manager. Le Driver Manager sera utilisé comme une << usine >> à connexions, pour créer des connexions JDBC.

JDBC offre la possibilité de créer et d'exécuter des requêtes (\emph{Statement}), aussi bien celles qui visent à modifier les données (\verb!CREATE!, \verb!INSERT!, \verb!UPDATE!, et \verb!DELETE! en \verb!SQL!) que celles qui se contentent de lire depuis la base de données (SELECT en SQL). En plus de cela, des procédures stockées peuvent être invoquées depuis une connexion JDBC. Les requêtes sont représentées par trois interfaces Java (\verb!Statement!, \verb!PreparedStatement! et \verb!CallableStatement!).

Pour se servir de JDBC toutefois, un connecteur (ou \emph{driver}) est nécessaire et dépend du système de gestion de bases de données utilisé. Dans notre cas, le connecteur MySQL est indispensable pour accéder à la base de données depuis notre application.
\subsection{Hypertext Markup Language}

\emph{HyperText Markup Language}, plus communément appelé HTML, est un langage standard utilisé pour la création de pages web. C'est un langage de balisage pour l'écriture de l'hypertexte, mais il permet également de structurer sémantiquement et de mettre en forme le contenu des pages, d'inclure des ressources multimédias (images, formulaires de saisie, etc.)

Il est souvent utilisé avec des langages de programmation comme JavaScript, et des formats de présentation comme CSS. La convention veut qu'HTML ne serve qu'à décrire la logique des documents et leur contenu, et que tout ce qui concerne le format du document soit précisé dans les feuilles de style CSS. JavaScript quant à lui, permet de rajouter un aspect interactif à la page statique obtenue par la combinaison d'HTML et de CSS.


\subsection{Cascading Style Sheets}
CSS est un langage de feuille de style, utilisé pour décrire l'apparence et la mise en forme de documents écrits dans un langage de balisage. Bien que plus généralement utilisé pour changer le style de pages web et d'interfaces utilisateur écrites en HTML ou en XHTML, ce langage peut s'appliquer sur n'importe quel document XML. Avec HTML et JavaScript, CSS est une technologie clef utilisée par la plupart des sites web pour créer des pages web visuellement attrayantes, des interfaces utilisateur pour les applications web et mobiles.

CSS a été principalement conçu pour permettre la séparation du contenu du document de sa présentation,
incluant la disposition des éléments, les couleurs, et les polices de texte. Cette séparation améliore l'accessibilité au contenu, donne plus de flexibilité au formattage, et permet à plusieurs pages HTML de partager une même mise en forme en les liant toutes à un fichier .css séparé, réduisant par là la complexité et les répétitions du contenu structuré. La séparation du CSS permet également de présenter la page de différentes façons selon les supports sur lesquels elles sont vues (grand ou petit écran, etc.)

La spécification CSS décrit une hiérarchie des priorités pour déterminer quel style va s'appliquer si un élément correspond à plusieurs règles. Ceci est à l'origine du nom, puisque les règles vont s'appliquer ``en cascade''.

\subsection{jQuery}

jQuery\cite{jquery} est une bibliothèque JavaScript libre et multi-plate-forme créée pour faciliter l'écriture de scripts côté client dans le code HTML des pages web.

La bibliothèque contient notamment les fonctionnalités suivantes :
 \begin{itemize}
\item Parcours et modification du DOM (y compris le support des sélecteurs CSS 1 à 3 et un support basique de XPath) ;
\item Événements ;
\item Effets visuels et animations ;
\item Manipulations des feuilles de style en cascade (ajout/suppression des classes, d'attributs…) ;
\item Ajax ;
\item Plugins ;
\item Utilitaires (version du navigateur web…).
\end{itemize}

\subsection{Jasypt}

Jasypt\cite{jasypt} est une bibliothèque Java, tenant son nom de \emph{Java Simplified Encryption}, cryptage simplifié en Java. Elle permet au développeur d'ajouter des possibilités de cryptage à son projet, sans demander de code élaboré ou d'expérience dans le domaine de la cryptographie.

Idéalement, les mots de passe des utilisateurs doivent être cryptés à l'inscription, et ne sont jamais déposés dans une structure de données en clair. Ils ne sont donc ni stockés ni manipulés par l'application avant d'avoir été transformés, à travers des algorithmes spécialisés associés à des << grains de sel >> aléatoires, en des chaînes de caractères dont il est extrêmement difficile de déduire la forme initiale. Jasypt gère un grand nombre de ces algorithmes, et se charge de crypter les mots de passe, puis de vérifier la correspondance entre un mot de passe entré et une empreinte déjà stockée en base de données, facilitant ainsi le processus de gestion de l'inscription et de l'authentification sécurisées.

Pour ce faire, il suffit d'utiliser les méthodes \verb!encryptPassword! et \verb!checkPassword! de la classe \verb!ConfigurablePasswordEncryptor!, en leur précisant l'algorithme de chiffrement. L'algorithme que nous avons choisi est l'algorithme SHA-256, qui produit des empreintes de 56 caractères.

\section{Outils supplémentaires}

\subsection{Git}

Git\cite{git} est un système de gestion de versions décentralisé, accordant une importance toute particulière à la vitesse, à l'intégrité des données, et aux flux de travaux distribués et non-linéaires. Le logiciel a été initialement conçu et développé par Linux Torvalds pour le développement du noyau Linux en 2005, et est devenu depuis le système de gestion de version le plus utilisé en développement logiciel.

Comme la plupart des systèmes de gestion de versions distribués, et à l'opposé de la plupart des systèmes client-serveur, chaque espace de travail Git est un \emph{repository} complet avec tout l'historique des modifications et toutes les fonctionnalités de suivi des versions, indépendamment d'un accès au réseau ou d'un serveur central. Tout comme le noyau Linux, Git est un logiciel libre, distribué selon les termes de la \emph{GNU General Public Licence} dans sa deuxième version.

Git est un logiciel en ligne de commande, mais en raison de sa grande utilisation, un grand nombre d'interfaces utilisateur a été développé par des développeurs indépendants. Il est également géré par un certain nombre d'IDE, dont Eclipse. 

\subsection{UMLet}

UMLet est un outil UML open-source écrit en java, conçu pour la création rapide des diagrammes UML. Il s'agit d'un outil de dessin et non d'un outil de conception, et ne fait aucun traitement sur le sens du diagramme créé. UMLet est distribué selon les termes de la \emph{GNU General Public Licence}.

UMLet contient les fonctionnalités suivantes :
\begin{itemize}
\item Une interface simple qui permet la modification des formes basiques à l'aide de propriétés texte. Le langage de balisage utilisé est très intuitif, facilitant à l'utilisateur l'apprentissage du logiciel.
\item La possibilité d'exporter les diagrammes en images en plusieurs formats d'image (jps, png, svg, etc.) ou de documents (pdf).
\item La possibilité de modifier des formes basiques grâce à un code simple en java.
\item Les éléments UML les plus importants sont tous disponibles (diagrammes de classes, de cas d'utilisation, de séquence, d'état, de déploiement, d'activité).
  \item La possibilité d'être utilisé comme logiciel indépendant ou comme plug-in du IDE Eclipse.
  \end{itemize}

Tous les diagrammes de ce mémoire ont été élaborés avec UMLet.

\subsection{\LaTeX}

\LaTeX\cite{latex} est un langage et un système de composition de documents créé par Leslie Lamport en 1983.
Il se distingue des traitements de texte (comme Microsoft Office ou OpenOffice) par le fait que l'utilisateur utilise du texte pur, par opposition au texte formaté. Le rédacteur se concentre donc sur la logique du contenu, tandis que la forme est interprétée pendant la phase de compilation du document.  Il s'agit de la méthode privilégiée pour la rédaction de documents scientifiques de tous domaines.

Ce mémoire a été rédigé en \LaTeX.

\section{Implémentation de la base de données}

Dans l'approche de conception que nous avons abordée, le schéma de la base de données peut être déduit depuis le modèle statique d'analyse
(voir figure \ref{msa})
qui représente les entités et les relations entre elles. Toutefois, il faut respecter certaines règles de passage pour s'assurer que le schéma relationnel exprime bien ce que nous avons modélisé durant la phase d'analyse.

Pour commencer, chaque classe doit être traduite par une table distincte en lui associant une clé primaire.


\subsection{Passage des associations}

Chaque association \emph{peut} être traduite en une relation distincte. Les clés primaires des relations participantes doivent y figurer comme \emph{clés étrangères}. La clé primaire de cette relation peut être formée par la concaténation des clés étrangères, comme elle peut être une autre clé candidate (par exemple une clé artificielle).\cite{meier}

Toutefois, traduire chaque association par une table ne conduit pas à un schéma de base de données relationnelle optimale. Ainsi, nous distinguerons le passage selon les trois types d'association (ceci est également valable dans le cas d'agrégations ou de composition) :

\begin{description}
\item[Les associations de type 1,1] peuvent s'exprimer dans les relations issues des classes participantes sans avoir besoin de créer une nouvelle. La clé primaire d'une relation devra apparaître comme clé étrangère dans l'autre.
\item[Les associations de type 1,*] peuvent également s'exprimer sans besoin de créer une nouvelle relation. Il nous suffit d'ajouter, dans la relation du côté de laquelle la multiplicité maximale est de 1, la clé étrangère de l'autre relation.
\item[Les associations de type *,*] doivent s'exprimer à l'aide d'une nouvelle relation contenant les clés primaires des deux relations participantes en clés étrangères. Si l'association est exprimée à l'aide d'une classe d'association, les attributs de cette classe apparaîtront dans la relation ajoutée.

  La cardinalité exprimée par un intervalle contenant le 0 (0..1 par exemple) sont exprimées par la possibilité d'entrer des valeurs null dans la base de données.
\end{description}

Dans le cas de notre schéma, nous avons fait une exception de la classe TestFinal, que nous avons exprimée comme une relation distincte au lieu d'en faire un attribut de la classe Module, pour des raisons techniques.

\begin{itemize}
\item Le fait que la multiplicité peut être de 0, cet attribut ne peut pas être une clé primaire du Module.
 \item MySQL facilite la créations de clés étrangères à partir de clés primaires par rapport à leur créations à partir d'attributs quelconques. En effet, les index en sont automatiquement générés dans ce cas. 
\end{itemize}


\subsection{Relation d'héritage}

La règle de passage d'une relation héritage vers le modèle relationnel est la suivante :

\begin{quotation}
Chaque classe dans une hiérarchie de généralisation
donne lieu à une table distincte. La clé primaire de la table ascendante
est aussi celle des tables au niveau inférieur.\cite{meier}
\end{quotation}

\subsubsection{Les différentes hiérarchies de généralisation}

Selon \cite{meier}, nous pouvons distinguer parmi les hiérarchies de généralisation  quatre catégories :
\begin{description}
\item[Type avec intersection et incomplet] où un élément de la classe mère peut correspondre à aucune, une ou plusieurs classes filles. Ceci est le cas de notre relation d'héritage à partir de la classe Membre. En effet, un membre peut être étudiant enseignant, tout comme il a la possibilité d'être inscrit sans avoir aucun de ces rôles.
\item[Type avec intersection et complet] où un élément de la classe mère doit correspondre à au moins une ou plusieurs classes filles.
\item[Type disjoint incomplet] où un élément de la classe mère doit correspondre au plus à une seule classe fille.
\item[Type disjoint complet] où un élément de la classe mère doit correspondre à une et une seule classe fille. Ceci est le cas de l'héritage de notre classe Séance.
\end{description}

Aucune règle de test n'est nécessaire pour garantir la propriété des ensembles spécialisés avec intersection non vide. L'implémentation des ensembles disjoints dans le modèle relationnel requiert en revanche une attention particulière. Une méthode consiste à introduire dans la table ascendante un nouvel attribut (catégorie, ou type) qui permet de réaliser une classification : chaque valeur de l'attribut désigne l'ensemble spécialisé représentant une catégorie. Dans le cas que nous avons, il faut en plus s'assurer qu'à chaque occurrence dans la table ascendante correspond une occurrence dans l'une des tables spécialisées et vice versa.

\newpage
\subsection {Modèle relationnel}

Le schéma de notre base de données relationnelle, en 3\up{ème} forme normale, comporte l'ensemble des schémas des relations suivantes:
\bigskip

\begin{fiche}
\begin{description}
\item[Membre](nom,prenom,mdpasse,\underline{email},estModerateur,sexe,dateNaissance)\\
dateInscription)
\item[Etudiant](\underline{emai}l,niveau)\\
\item[Enseignant](\underline{email},estResponsable)\\
\item[TestFinal](\underline{clefTest})\\
\item[Modules](\underline{clefModule},nom,description,niveau,estVisible,dateParution,responsable,\\auteur,
tuteur,testFinal)
\item[Inscription Module](\underline{email},\underline{ clefModule}, note)\\
\item[Cours](\underline{clefCours},nom,clefModule,faq)\\
\item[Seance](\underline{clefSeance}, clefCours,indice,nom,typeSeance)\\
\item[Lecon](\underline{clefLecon}, urlFichier,extension)\\
\item[Question](\underline{clefQuestion},contenu)\\
\item[Quiz](\underline{clefQuiz})\\
\item[Reponse](\underline{clefReponse},clefQuestion, contenu,estCorrecte)\\
\item[QuestionTest](\underline{clefTest},\underline{clefQuestion})\\
\item[QuestionQuiz](\underline{clefQuiz},clefQuestion)\\
\item[ContenuAdditionnel](\underline{clefCA},clefSeance,nom,urlFichier,extension)\\
\item[Topic](\underline{clefTopic},clefSeance,titreTopic, dateCreation)\\
\item[Message](\underline{clefMessage},clefTopic,auteurMessage,datePublication,dateModif,contenu)
\end{description} 
\end{fiche}


\newpage
\begin{figure}[H]
    \makebox[\linewidth]{
        \includegraphics[scale=0.3]{figures/Database.png} 
        }
 \caption{Diagramme entité-association étendu de la base de données.} 
 \label{bdd} 
 \end{figure}	
\newpage

\lstset{
language=Java,
basicstyle=\small\ttfamily,
showspaces=false,
showstringspaces=false,
showtabs=false,
frame=single,
tabsize=2,
breaklines=true,
%extendedchars=\true
%inputencoding=utf8x
%breakatwhitespace=true,
keywordstyle=\color{blue},
identifierstyle=,
commentstyle=\color{gray},
stringstyle=\color{red}
}

\section{Interfaces de quelques fonctionnalités}

\begin{figure}[h]
    \makebox[\linewidth]{
        \includegraphics[scale=0.3]{figures/implementation/accueil.png} 
        }
 \caption{Page d'accueil du site.} 
 \label{accueil} 
\end{figure}

\subsection{S'inscrire}
Dans la figure \ref{inscr}, nous avons différentes interfaces selon les cas lors de l'inscription. La requête d'inscription est analysée par la Servlet qui la reçoit, et retourne une liste d'erreurs à afficher en cas d'échec (comme le cas d'erreur du formulaire vide illustré en haut, à gauche de la figure), ou effectue l'opération en cas de succès (comme montré à droite de la figure, après soumission du formulaire rempli).
\begin{figure}[h]
    \makebox[\linewidth]{
        \includegraphics[scale=0.3]{figures/implementation/inscription.png} 
        }
 \caption{Interfaces du cas d'utilisation << S'inscrire >>} 
 \label{inscr} 
\end{figure}

Ci-dessous la méthode du contrôleur. (cette méthode fait appel à des méthodes privées et des variables globales non-inclues dans ce code.)

\begin{table}[H]
\lstinputlisting{code/MembreCtr.java}
\end{table}
\subsection{Créer un module}

Cette fonctionnalité permet à un enseignant-responsable de créer un module. La figure \ref{creermod} représente l'interface telle que présentée à l'utilisateur. En cas d'échec, cette page est affichée avec les erreurs ayant survenu à l'instar du cas précédent. En cas de réussite, l'utilisateur est redirigé vers la page du module nouvellement créé (figure \ref{pagepooj}).

\begin{figure}[h]
    \makebox[\linewidth]{
        \includegraphics[scale=0.3]{figures/implementation/creationpooj.png} 
        }
 \caption{Interface du cas d'utilisation << Créer un module >>} 
 \label{creermod} 
\end{figure}

Cette méthode décrit globalement le traitement du cas
\begin{table}[H]
\lstinputlisting{code/ModuleCtr.java}
\end{table}
\begin{figure}[H]
    \makebox[\linewidth]{
        \includegraphics[scale=0.3]{figures/implementation/pagepooj.png} 
        }
 \caption{Interface résultat du cas d'utilisation << Créer un module >>} 
 \label{pagepooj} 
\end{figure}

\subsection{Rechercher un module}

Dans le cas de la recherche, nous avons utilisé la bibliothèque Java << Appache Lucene >>\cite{lucene}. Il s'agit d'une bibliothèque très puissante permettant d'intégrer à une application web son propre moteur de recherche (au lieu d'utiliser des moteurs existants tels que Google.)

Pour ce faire, il faut en premier lieu indexer les informations des modules sur lesquelles l'utilisateur peut faire ses recherches. Ceci se fait, dans notre cas, au déploiement de l'application, dans la méthode \verb!contextInitialized(ServletContextEvent event)! d'une classe implémentant l'interface \verb!ServletContextListener!.

Il faut toutefois préciser au conteneur qu'il doit exécuter cette méthode à son déploiement, en ajoutant une balise \verb!<listener></listener>! qui regroupe les informations propres à cette classe dans le fichier web.xml.

\begin{figure}[h]
    \makebox[\linewidth]{
        \includegraphics[scale=0.3]{figures/implementation/recherche.png} 
        }
 \caption{Interface résultat du cas d'utilisation << Rechercher un module >>} 
 \label{bdd} 
\end{figure}

Lorsqu'une requête arrive, une Servlet java se charge de retirer l'index du contexte de l'application, et fait une recherche dessus. La méthode étant assez longue, nous mettrons juste le bloc chargé du traitement de la requête de recherche :
\begin{table}[H]
\lstinputlisting{code/RechercheServlet.java}
\end{table}
