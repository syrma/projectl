
		IndexReader reader = null;
		try{
			Query q = new MultiFieldQueryParser(new String[]{"nom","description","responsable","auteur","tuteur"}, analyzer).parse(query);
			reader = DirectoryReader.open(index);
			IndexSearcher searcher = new IndexSearcher(reader);
			TopDocs to = searcher.search(q, 15);
			ScoreDoc[] hits = to.scoreDocs; 

			request.setAttribute(ATTR_RECHERCHE,"Trouvé " + hits.length + " résultat(s).");

			ArrayList<Module> listeModules = new ArrayList<Module>(); 
			for(int i=0;i<hits.length;++i) {
				int docId = hits[i].doc;
				Document d = searcher.doc(docId);
				listeModules.add(dao.read(Integer.parseInt(d.get("id"))));
			}

			request.setAttribute(ATTR_LISTE, listeModules);
		}catch(ParseException e){
			request.setAttribute(ATTR_RECHERCHE, "Un problème est survenu lors du traitement de votre requête.");
		}
