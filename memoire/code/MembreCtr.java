	public void inscrire(){
		verifierEmail();
		verifierMdpasse();
		verifierIdentite();
		
		if(erreurs.size()==0){
			try{
				membre.setEstModerateur(false);
				membre.setDateInscription(new java.util.Date());
				dao.create(membre);
				resultat = "Inscription réussie.";
			}catch(DAOException e){
				e.printStackTrace();
				resultat = "L'inscription n'a pas pu se terminer correctement, merci de réessayer.";
			}
		}
		else{
			resultat = "Échec de l'inscription.";
		}
		requete.setAttribute("listeErreurs", erreurs);
		requete.setAttribute("resultat", resultat);
		
	}

