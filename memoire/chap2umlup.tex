\chapter{UML et UP}

\section{Unified Modeling Language}

Comme il a été mentionné dans \cite{zanellaligier}, UML (\emph{Unified Modeling Language}\cite{omg}) est un langage de modélisation des applications construites à partir d'objets. Il est basé sur différentes méthodes orientées objet qui visent à modéliser les objets constituant une application. Il est né de la fusion des trois plus importantes méthodes de modélisation objet : OMT (\emph{Object Modeling Technique}), BOOCH, et OOSE (\emph{Object Oriented Software Engineering}). C'est devenu un standard international de l'OMG (\emph{Object Management Group}). 


Il est également précisé dans \cite{audibert} qu'UML n'est pas une méthode (i.e. une description normative des étapes de la modélisation) : ses auteurs ont en effet estimé qu'il n'était pas opportun de définir une méthode en raison de la diversité des cas particuliers. Ils ont préféré se borner à définir un langage graphique qui permet de représenter, de communiquer les divers aspects d'un système d'information (aux graphiques sont bien sûr associés des textes qui expliquent leur contenu). UML est donc un métalangage car il fournit les éléments permettant de construire le modèle qui, lui, sera le langage du projet.

Il est impossible de donner une représentation graphique complète d'un logiciel, ou de tout autre système complexe, de même qu'il est impossible de représenter entièrement une statue (à trois dimensions) par des photographies (à deux dimensions). Mais il est possible de donner sur un tel système des vues partielles, analogues chacune à une photographie d'une statue, et dont la juxtaposition donnera une idée utilisable en pratique sans risque d'erreur grave.
%\newpage
UML 2.0 comporte ainsi treize types de diagrammes représentant autant de vues distinctes pour
représenter des concepts particuliers du système d'information. Ils se répartissent en deux grands groupes :(la figure qui suit est tirée de \cite{roques})


	\begin{figure}[H]
	\includegraphics[width = \textwidth]{figures/umlup/diags_uml.png}
	\caption{Les treize types de diagrammes d'UML.}
	\label{diagsUML}
	\end{figure}
        
Ces diagrammes, d'une utilité variable selon les cas, ne sont pas nécessairement tous produits à l'occasion d'une modélisation. Les plus utiles pour la maîtrise d'ouvrage sont les diagrammes d'activités, de
cas d'utilisation, de classes, d'objets, de séquence et d'états-transitions. Les diagrammes de composants,
de déploiement et de communication sont surtout utiles pour la maîtrise d'œuvre à qui ils permettent
de formaliser les contraintes de la réalisation et la solution technique.


\paragraph{Diagramme de classes (Class diagram)}
Le diagramme de classes est généralement considéré comme le plus important dans un
développement orienté objet. Il représente l'architecture conceptuelle du système : il décrit les classes
que le système utilise, ainsi que leurs liens, que ceux-ci représentent un emboîtage conceptuel (héritage)
ou une relation organique (agrégation).
Le diagramme de classes est le seul obligatoire lors d'une telle modélisation.
Alors que le diagramme de cas d'utilisation montre un système du point de vue des acteurs, le
diagramme de classes en montre la structure interne. Il permet de fournir une représentation abstraite
des objets du système qui vont interagir ensemble pour réaliser les cas d'utilisation. Il est important de
noter qu'un même objet peut très bien intervenir dans la réalisation de plusieurs cas d'utilisation. Les cas
d'utilisation ne réalisent donc pas une partition 1 des classes du diagramme de classes. Un diagramme de
classes n'est donc pas adapté (sauf cas particulier) pour détailler, décomposer, ou illustrer la réalisation
d'un cas d'utilisation particulier.
Il s'agit d'une vue statique car on ne tient pas compte du facteur temporel dans le comportement du
système. Le diagramme de classes modélise les concepts du domaine d'application ainsi que les concepts
internes créés de toutes pièces dans le cadre de l'implémentation d'une application. Chaque langage de
Programmation Orienté Objets donne un moyen spécifique d'implémenter le paradigme objet (pointeurs
ou pas, héritage multiple ou pas, etc.), mais le diagramme de classes permet de modéliser les classes du
système et leurs relations indépendamment d'un langage de programmation particulier.
Les principaux éléments de cette vue statique sont les classes et leurs relations : association, généralisation et plusieurs types de dépendances, telles que la réalisation et l'utilisation.

\paragraph{Diagramme d'objets (Object diagram)}
Le diagramme d'objets permet d'éclairer un diagramme de classes en l'illustrant
par des exemples. Il est, par exemple, utilisé pour vérifier l'adéquation d'un diagramme de classes à
différents cas possibles.
Un diagramme d'objets représente des objets (i.e. instances de classes) et leurs liens (i.e. instances de
relations) pour donner une vue de l'état du système à un instant donné. Un diagramme d'objets permet,
selon les situations, d'illustrer le modèle de classes (en montrant un exemple qui explique le modèle),
de préciser certains aspects du système (en mettant en évidence des détails imperceptibles dans le
diagramme de classes), d'exprimer une exception (en modélisant des cas particuliers, des connaissances
non généralisables . . .), ou de prendre une image (\emph{snapshot}) d'un système à un moment donné. Le
diagramme de classes modélise les règles et le diagramme d'objets modélise des faits.
\begin{figure}[H]
\begin{center}
\includegraphics[width = \linewidth]{figures/umlup/page46}
\caption{Exemple de diagramme de classes et de diagramme d'objets associé.}
\label{diag-obj}
\end{center}
\end{figure}
Par exemple, le diagramme de classes de la figure \ref{diag-obj} montre qu'une entreprise emploie au moins
deux personnes et qu'une personne travaille dans, au plus, deux entreprises. Le diagramme d'objets
modélise lui une entreprise particulière (OSKAD) qui emploie trois personnes.
Un diagramme d'objets ne montre pas l'évolution du système dans le temps. Pour représenter une
interaction, il faut utiliser un diagramme de communication.

\paragraph{Diagramme de composants (Component diagram)}

La relation de dépendance est utilisée dans les diagrammes de composants pour indiquer qu'un
élément de l'implémentation d'un composant fait appel aux services offerts par les éléments d'implémentation d'un autre composant (\ref{diag-comp-2}).

Lorsqu'un composant utilise l'interface d'un autre composant, on peut utiliser la représentation de
la figure \ref{diag-comp-1}

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{figures/umlup/8-6-page106}
\end{center}
\caption{Exemple de diagramme montrant les dépendances entre composants.}
\label{diag-comp-3}
\end{figure}


\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{figures/umlup/8-3-page104}
\end{center}
\caption{Représentation classique d'un composant et de ses interfaces requise (représenté par un demi-
cercle) et offerte (représentée par un cercle).}
\label{diag-comp-1}
\end{figure}



 en imbriquant le demi-cercle d'une interface requise dans le cercle de l'interface offerte
correspondante (figure \ref{diag-comp-2}).

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{figures/umlup/8-5-page106}
\end{center}
\caption{Représentation de l'implémentation d'un composant complexe contenant des sous-composants.}
\label{diag-comp-2}
\end{figure}


\paragraph{Diagramme de déploiement (Deployment diagram)}
Un diagramme de déploiement décrit la disposition physique des ressources matérielles qui composent 
le système et montre la répartition des composants sur ces matériels. Chaque ressource étant
matérialisée par un nœud, le diagramme de déploiement précise comment les composants sont répartis
sur les nœuds et quelles sont les connexions entre les composants ou les nœuds. Les diagrammes de
déploiement existent sous deux formes : spécification et instance.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{figures/umlup/8-12-page109}
\end{center}
\caption{Exemple de diagramme de déploiement illustrant la communication entre plusieurs nœuds.}
\label{diag-dep}
\end{figure}

Dans un diagramme de déploiement, les associations entre nœuds sont des chemins de communication qui permettent l'échange d'informations (figure \ref{diag-dep}).


\paragraph{Diagramme de paquetages (Package diagram)}

Les diagrammes de paquetages peuvent utiliser des paquetages pour illustrer les différentes couches de l'architecture en couches d'un système logiciel. Les dépendances entre paquetages peuvent être parés d'étiquettes ou de stéréotypes pour indiquer les mécanismes de communication entre les couches.

\paragraph{Diagramme de structures composites (Composite structure diagram)}

Le diagramme des structures composites est apparu dans la spécification d'UML 2.0. Les éléments clés du diagramme de structure composite sont les classifieurs structurés, les parties, les ports, les connecteurs et les collaborations.

\paragraph{Diagramme de cas d'utilisation (Use case diagram)}

Le diagramme de cas d'utilisation représente la structure des grandes fonctionnalités
nécessaires aux utilisateurs du système. C'est le premier diagramme du modèle UML, celui où s'assure
la relation entre l'utilisateur et les objets que le système met en œuvre.
Bien souvent, la maîtrise d'ouvrage et les utilisateurs ne sont pas des informaticiens. Il leur faut
donc un moyen simple d'exprimer leurs besoins. C'est précisément le rôle des diagrammes de cas
d'utilisation qui permettent de recueillir, d'analyser et d'organiser les besoins, et de recenser les grandes
fonctionnalités d'un système. Il s'agit donc de la première étape UML d'analyse d'un système.
Un diagramme de cas d'utilisation capture le comportement d'un système, d'un sous-système, d'une
classe ou d'un composant tel qu'un utilisateur extérieur le voit. Il scinde la fonctionnalité du système en
unités cohérentes, les cas d'utilisation, ayant un sens pour les acteurs. Les cas d'utilisation permettent
d'exprimer le besoin des utilisateurs d'un système, ils sont donc une vision orientée utilisateur de ce
besoin au contraire d'une vision informatique.
Il ne faut pas négliger cette première étape pour produire un logiciel conforme aux attentes des
utilisateurs. Pour élaborer les cas d'utilisation, il faut se fonder sur des entretiens avec les utilisateurs.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{figures/umlup/2-5-page27}
\end{center}
\caption{Exemple simplifié de diagramme de cas d'utilisation modélisant une borne d'accès à une banque.}
\label{diag-cas}
\end{figure}

Comme le montre la figure \ref{diag-cas}, la frontière du système est représentée par un cadre. Le nom du
système figure à l'intérieur du cadre, en haut. Les acteurs sont à l'extérieur et les cas d'utilisation à
l'intérieur.

\paragraph{Diagramme d'activités (Activity diagram)}
Le diagramme d'activités n'est autre que la transcription dans UML de la représentation du processus telle qu'elle a été élaborée lors du travail qui a préparé la modélisation : il montre
l'enchaînement des activités qui concourent au processus.

Les diagrammes d'activités permettent de mettre l'accent sur les traitements.
 Ils sont donc particulièrement adaptés à la modélisation du cheminement de flots de contrôle et de flots de données. Ils
permettent ainsi de représenter graphiquement le comportement d'une méthode ou le déroulement d'un
cas d'utilisation.
Les diagrammes d'activités sont relativement proches des diagrammes
 d'états-transitions dans leur
présentation, mais leur interprétation 
est sensiblement différente. Les diagrammes d'états-transitions
sont orientés vers des systèmes réactifs, mais ils ne donnent pas une vision satisfaisante d'un traitement
faisant intervenir plusieurs classeurs et doivent être complétés, par exemple, par des diagrammes de
séquence. Au contraire, les diagrammes d'activités ne sont pas spécifiquement rattachés à un classeur
particulier. Ils permettent de spécifier des traitements a priori séquentiels et offrent une vision très proche
de celle des langages de programmation impératifs comme C++ ou Java.
Dans la phase de conception, les diagrammes d'activités sont particulièrement adaptés à la description
des cas d'utilisation. Plus précisément, ils viennent illustrer et consolider la description textuelle des
cas d'utilisation. De plus, leur représentation sous forme d'organigrammes les rend
facilement intelligibles et beaucoup plus accessibles que les diagrammes d'états-transitions.
Les diagrammes d'activités sont également utiles dans la phase de réalisation car ils permettent une
description si précise des traitements qu'elle autorise la génération automatique du code.

\begin{figure}[H]
\begin{center}
\includegraphics{figures/umlup/6-2-page81}
\end{center}
\caption{Exemple de diagramme d'activités modélisant le fonctionnement d'une borne bancaire.}
\label{diag-activ}
\end{figure}

\paragraph{Diagramme d'états-transitions (State machine diagram)}
Le diagramme d'états-transitions représente la façon dont évoluent (i.e. cycle de vie) les
objets appartenant à une même classe. La modélisation du cycle de vie est essentielle pour représenter
et mettre en forme la dynamique du système.
Les diagrammes d'états-transitions d'UML décrivent le comportement interne d'un objet à l'aide
d'un automate à états finis. Ils présentent les séquences possibles d'états et d'actions qu'une instance de
classe peut traiter au cours de son cycle de vie en réaction à des événements discrets (de type signaux,
invocations de méthode).
Ils spécifient habituellement le comportement d'une instance de classeur (classe ou composant), mais
parfois aussi le comportement interne d'autres éléments tels que les cas d'utilisation, les sous-systèmes,
les méthodes.
Le diagramme d'états-transitions est le seul diagramme, de la norme UML, à offrir une vision
complète et non ambiguë de l'ensemble des comportements de l'élément auquel il est attaché. En effet,
un diagramme d'interaction n'offre qu'une vue partielle correspondant à un scénario sans spécifier
comment les différents \emph{scénarii} interagissent entre eux.
La vision globale du système n'apparaît pas sur ce type de diagramme puisqu'ils ne s'intéressent
qu'à un seul élément du système indépendamment de son environnement.
Concrètement, un diagramme d'états-transitions est un graphe qui représente un automate à états
finis, c'est-à-dire une machine dont le comportement des sorties ne dépend pas seulement de l'état de
ses entrées, mais aussi d'un historique des sollicitations passées.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.5]{figures/umlup/diag-etat}
\end{center}
\caption{Un diagramme d'états-transitions simple.}
\label{diag-etat}
\end{figure}


\paragraph{Diagramme de séquence (Sequence diagram)}
Le diagramme de séquence représente la succession chronologique des opérations
réalisées par un acteur. Il indique les objets que l'acteur va manipuler et les opérations qui font passer
d'un objet à l'autre. On peut représenter les mêmes opérations par un diagramme de communication
, graphe dont les nœuds sont des objets et les arcs (numérotés selon la chronologie) les
échanges entre objets. En fait, diagramme de séquence et diagramme de communication sont deux vues
différentes mais logiquement équivalentes (on peut construire l'une à partir de l'autre) d'une même
chronologie. Ce sont des diagrammes d'interaction.

\begin{figure}[H]
\begin{center}
\includegraphics[scale =0.6]{figures/umlup/diag-seq}
\end{center}
\caption{Diagramme de séquence d'un système de pilotage.}
\label{diag-seq}
\end{figure}

\paragraph{Diagramme de communication (Communication diagram)}
Contrairement à un diagramme de séquence, un diagramme de communication rend compte de l'organisation spatiale des participants à l'interaction, il est souvent utilisé pour illustrer un cas d'utilisation
ou pour décrire une opération.
%\begin{figure}[H]
%\begin{center}
%\includegraphics[scale=0.6]{figures/umlup/diag-com}
%\end{center}
%\caption{Diagramme de communication d'un système de pilotage.}
%\label{diag-seq}
%\end{figure}

\paragraph{Diagramme global d'interaction (Interaction overview diagram)}

Les diagrammes globaux d'interaction définissent des interactions par une variante des diagrammes d'activité, d'une manière qui permet une vue d'ensemble de flux de contrôle.

Ils se concentrent sur la vue d'ensemble de flux de contrôle où les nœuds sont des interactions ou \emph{Interaction Uses}.

%\paragraph{ Diagramme de temps (Timing diagram)}

%Un diagramme de temps est un diagramme d'interaction où l'attention est portée sur les contraintes temporelles dans le langage UML2.
%Les diagrammes de temps sont utilisés pour explorer le comportement des objets d'un système à travers une période de temps.

\bigskip
Comme il a été spécifié dans \cite{zanellaligier}, UML permet d'exprimer des modèles objet en faisant abstraction de leur implémentation, c'est-à-dire que ce type de modèle est valable pour n'importe quel langage de programmation. UML est un langage qui s'appuie sur un métamodèle, un modèle de plus haut niveau qui définit les éléments d'UML (les concepts utilisables) et leur sémantique (leur signification et leur mode d'utilisation). UML est un langage, donc un formalisme pour représenter des concepts. Il ne suffit pas à produire des logiciels de qualité, mais son utilisation, largement répandue, a prouvé son utilité dans la spécification d'une application.

UML est un langage riche dont l'utilisation nécessite un apprentissage et un certain pragmatisme. L'utilisation systématique de tous les modèles n'est pas recommandée, ce n'est pas un gage de qualité. L'utilisation d'un type de diagramme doit être adapté au niveau d'abstraction que l'on souhaite modéliser. L'utilisation judicieuse des différents types de diagrammes doit intervenir à des moments précis du cycle de développement. Ce canevas d'utilisation de la notation fait partie du processus de développement. 



\section{Unified Process}

D'après \cite{zanellaligier}, historiquement, suite à la standardisation d'UML, une vaste action de standardisation du processus qui accompagne la notation a été entreprise, d'ailleurs par les mêmes protagonistes. Le résultat est connu sous le nom de processus unifié : UP. (voir \cite{omg})

Ce processus est disponible sous la forme d'une norme, concrétisée par différents produits commerciaux. A ce jour, l'implémentation la plus largement utilisée est celle d'IBM/Rational, connue sous le nom de RUP (\emph{Rational Unified Process}).

Concrètement, le produit se présente sous la forme d'un gigantesque Intranet composé de plusieurs milliers de pages truffées de liens croisés, dans lesquelles un soin tout particulier a été apporté pour unifier la terminologie et la mise en forme.

L'originalité majeure de ce processus est surtout visible dans les points suivants : 

\begin{itemize}
\item une approche itérative et incrémentale, qui implique que les différents groupes d'activités (appelés disciplines) soient parcourus. à chaque itération. Ceci a pour effet concret d'ajouter une seconde dimension de nature organisationnelle à la dimension temporelle principale qui constitue la colonne vertébrale des processus traditionnels ; 
\item une utilisation intensive de la notation UML, à la fois pour décrire le processus lui-même, et pour enrichir les livrables produits au cours du processus.De 40\% à 60\% de l'effort déployé consiste à produire des modèles UML.
\end{itemize}

D'autres points plus ou moins originaux caractérisent UP, comme une gestion systématique des risques, la prise en compte d'un processus de gestion du changement au cœur même du processus, et la mise en place d'un méta-processus de contrôle destiné à améliorer l'efficacité de la mise en œuvre du processus lui-même d'une itération à l'autre. 

Ces derniers points sont tous très pertinents et contribuent à la mise en place d'une véritable industrialisation du développement, mais chacun d'entre eux existait déjà dans d'autres processus avant la naissance d'UP. Ils ont été simplement repris et intégrés.

L'ensemble de la démarche s'articule autour du concept du cas d'utilisation (\emph{use case}) qui constitue véritablement la cheville ouvrière de tout le processus, depuis l'estimation initiale des coûts en passant par le découpage en itérations et l'organisation des tests. 

\subsection{Activités}

\paragraph{Expression des besoins}
L'expression des besoins permet de
\begin{itemize}
\item inventorier les besoins principaux et fournir une liste de leurs fonctions
\item recenser les besoins fonctionnels (du point de vue de l'utilisateur) qui conduisent à
l'élaboration des modèles de cas d'utilisation
\item appréhender les besoins non fonctionnels (technique) et livrer une liste des exigences.
Le modèle de cas d'utilisation présente le système du point de vue de l'utilisateur et représente
sous forme de cas d'utilisation et d'acteur, les besoins du client.
\end{itemize}

\paragraph{Analyse}
L'objectif de l'analyse est d'accéder à une compréhension des besoins et des exigences du client.
Il s'agit de réaliser des spécifications permettant de concevoir la solution.
Un modèle d'analyse livre une spécification complète des besoins issus des cas d'utilisation et les
Structures sous une forme qui facilite la compréhension (scénarios), la préparation (définition de
l'architecture), la modification et la maintenance du futur système. Il peut être considéré comme
une première ébauche du modèle de conception.

\paragraph{Conception}
La conception permet d'acquérir une compréhension approfondie des contraintes liées au langage
de programmation, à l'utilisation des composants et au système d'exploitation. Elle détermine les
principales interfaces et les transcrit à l'aide d'une notation commune.
Elle constitue un point de départ à l'implémentation, 
\begin{itemize}
\item elle décompose le travail d'implémentation en sous-système,
\item elle crée une abstraction transparente de l'implémentation.
\end{itemize}

\paragraph{Implémentation}
L'implémentation est le résultat de la conception pour implémenter le système sous formes de
composants, c'est-à-dire, de code source, de scripts, de binaires, d'exécutables et d'autres éléments
du même type. Les objectifs principaux de l'implémentation sont de planifier les intégrations des
composants pour chaque itération, et de produire les classes et les sous-systèmes sous formes de
codes sources.

\paragraph{Test}
Les tests permettent de vérifier des résultats de l'implémentation en testant la construction. Pour
mener à bien ces tests, il faut les planifier pour chaque itération, les implémenter en créant des
cas de tests, effectuer ces tests et prendre en compte le résultat de chacun.


\begin{figure}[H]
\begin{center}
\includegraphics[scale =0.6]{figures/umlup/diag-up}
\end{center}
\caption{Les phases et les activités de UP.}
\label{phasesactivites}
\end{figure}


\subsection{Phases}
\paragraph{Analyse des besoins}
L'analyse des besoins donne une vue du projet sous forme de produit fini.
Cette phase porte essentiellement sur les besoins principaux (du point de vue de l'utilisateur),
l'architecture générale du système, les risques majeurs, les délais et les coûts (On met en place le
projet).
Elle répond aux questions suivantes 
\begin{itemize}
\item que va faire le système ? Par rapport aux utilisateurs principaux, quels services va-t-il
rendre?
\item quelle va être l'architecture générale (cible) de ce système
\item quels vont être : les délais, les coûts, les ressources, les moyens à déployer?
\end{itemize}

\paragraph{Élaboration}
L'élaboration reprend les éléments de la phase d'analyse des besoins et les précise pour arriver à
une spécification détaillée de la solution à mettre en œuvre.
L'élaboration permet de préciser la plupart des cas d'utilisation, de concevoir l'architecture du
système et surtout de déterminer l'architecture de référence.
Au terme de cette phase, les chefs de projet doivent être en mesure de prévoir les activités et
d'estimer les ressources nécessaires à l'achèvement du projet.
Les taches à effectuer dans la phase élaboration sont les suivantes 
\begin{itemize}
\item créer une architecture de référence
\item identifier les risques, ceux qui sont de nature à bouleverser le plan, le coût et le calendrier
\item définir les niveaux de qualité à atteindre
\item formuler les cas d'utilisation pour couvrir les besoins fonctionnels et planifier la phase de
construction
\item élaborer une offre abordant les questions de calendrier, de personnel et de budget
\end{itemize}

\paragraph{Construction}
La construction est le moment où l'on construit le produit (architecture= produit complet). Le
produit contient tous les cas d'utilisation que les chefs de projet, en accord avec les utilisateurs
ont décidé de mettre au point pour cette version.
\paragraph{Transition}
Un groupe d'utilisateurs essaye le produit et détecte les anomalies et défauts.
Cette phase suppose des activités comme la formation des utilisateurs clients, la mise en œuvre
d'un service d'assistance et la correction des anomalies constatées.\cite{choutri}


\section*{Conclusion}

Ce chapitre nous a permis de reprendre les points essentiels concernant le langage UML et le processus unifié. Ces deux outils seront nécessaires à la conception de notre projet, puisque nous suivrons UP, selon l'approche de Pascal Roques\cite{roques}, pour aboutir à notre résultat souhaité. L'approche que nous avons choisie se résume dans le schéma suivant\cite{audibert} :

\begin{figure}[H]
    \makebox[\linewidth]{
        \includegraphics[width=\textwidth]{figures/schemaglobal.png} 
        }
 \caption{Chaîne complète de la démarche de modélisation du besoin jusqu'au code.} 
 \label{schemaglobal} 
 \end{figure}	
