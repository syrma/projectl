USE ProjetLicence;
DROP TABLE IF EXISTS  Message, Topic, ContenuAdditionnel, QuestionQuiz, QuestionTest,Reponse,Quiz, Question, Lecon, Seance, Cours, InscriptionModule, Modules, TestFinal, Enseignant,Etudiant, Membre ;

CREATE TABLE Membre(
	nom VARCHAR(25) NOT NULL,
	prenom VARCHAR(25) NOT NULL,
	mdpasse VARCHAR(56) NOT NULL,
	email VARCHAR(100) NOT NULL,
	estModerateur BOOLEAN NOT NULL,
	sexe ENUM('f','m'),
	dateNaissance DATE,
	dateInscription DATE NOT NULL,
	urlImage TEXT,
	PRIMARY KEY(email)
	)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE Etudiant(
	email VARCHAR(100) NOT NULL,
	niveau ENUM('1', '2','3'),
	PRIMARY KEY(email),
	CONSTRAINT membre1 FOREIGN KEY(email) REFERENCES Membre(email)
	)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE Enseignant(
	email VARCHAR(100) NOT NULL,
	estResponsable BOOLEAN NOT NULL,
	PRIMARY KEY(email),
	CONSTRAINT membre2 FOREIGN KEY(email) REFERENCES Membre(email)
	)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE TestFinal(
	clefTest INT NOT NULL AUTO_INCREMENT,
	PRIMARY KEY(clefTest)
	)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE Modules(
	clefModule INT NOT NULL AUTO_INCREMENT,
	nom VARCHAR(100) NOT NULL,
	description TEXT NOT NULL,
	niveau ENUM('1', '2', '3'),
	estVisible BOOLEAN NOT NULL,
	dateParution DATE NOT NULL,
	responsable VARCHAR(100) NOT NULL,
	auteur VARCHAR(100),
	tuteur VARCHAR(100),
	testFinal INT,
	urlImage TEXT,

	PRIMARY KEY(clefModule),
	CONSTRAINT responsable12 FOREIGN KEY (responsable) REFERENCES Enseignant (email),
	CONSTRAINT auteur13 FOREIGN KEY (auteur) REFERENCES Enseignant (email),
	CONSTRAINT tuteur14 FOREIGN KEY (tuteur) REFERENCES Enseignant (email),    
	CONSTRAINT test15 FOREIGN KEY (testFinal) REFERENCES TestFinal (clefTest)   
	)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;
	
CREATE TABLE InscriptionModule(
	email VARCHAR(100) NOT NULL,
	clefModule INT NOT NULL,
	note INT,
	PRIMARY KEY(email,clefModule),
	CONSTRAINT etudiant10 FOREIGN KEY (email) REFERENCES Etudiant (email),
	CONSTRAINT modules11 FOREIGN KEY (clefModule) REFERENCES Modules (clefModule)
	)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;
	
CREATE TABLE Cours(
	clefCours INT NOT NULL AUTO_INCREMENT,
	nom VARCHAR(100),
	indice INT NOT NULL,
	clefModule INT NOT NULL,
	faq TEXT,
	PRIMARY KEY(clefCours),
	CONSTRAINT modules16 FOREIGN KEY(clefModule) REFERENCES Modules(clefModule)
	)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE Seance(
	clefSeance INT NOT NULL AUTO_INCREMENT, 
	clefCours INT NOT NULL,
	indice INT NOT NULL,
	nom VARCHAR(100) NOT NULL,
	typeSeance ENUM('Lecon', 'Quiz') ,
	PRIMARY KEY(clefSeance),
	CONSTRAINT cours17 FOREIGN KEY(clefCours) REFERENCES Cours(clefCours)
	)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE Lecon(
	clefLecon INT NOT NULL, 
	urlFichier TEXT NOT NULL,
	extension VARCHAR(5) NOT NULL,
	PRIMARY KEY(clefLecon),
	CONSTRAINT seance18 FOREIGN KEY(clefLecon) REFERENCES Seance(clefSeance)
	)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE Question(
	clefQuestion INT NOT NULL AUTO_INCREMENT,
	contenu TEXT NOT NULL,
	PRIMARY KEY(clefQuestion)
	)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE Quiz(
	clefQuiz INT NOT NULL, 
	PRIMARY KEY(clefQuiz),
	CONSTRAINT seance19 FOREIGN KEY(clefQuiz) REFERENCES Seance(clefSeance)
	)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE Reponse(
	clefReponse INT NOT NULL AUTO_INCREMENT,
	clefQuestion INT NOT NULL, 
	contenu TEXT NOT NULL,
	estCorrecte BOOLEAN NOT NULL,
	PRIMARY KEY(clefReponse),
	CONSTRAINT question19 FOREIGN KEY(clefQuestion) REFERENCES Question(clefQuestion)
	)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;
	
CREATE TABLE QuestionTest(
	clefTest INT NOT NULL,
	clefQuestion INT NOT NULL,
	PRIMARY KEY(clefTest,clefQuestion),
	CONSTRAINT test6 FOREIGN KEY(clefTest) REFERENCES TestFinal(clefTest),
	CONSTRAINT question7 FOREIGN KEY(clefQuestion) REFERENCES Question(clefQuestion)
	)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE QuestionQuiz(
	clefQuiz INT NOT NULL,
	clefQuestion INT NOT NULL,
	PRIMARY KEY(clefQuiz,clefQuestion),
	CONSTRAINT quiz8 FOREIGN KEY(clefQuiz) REFERENCES Quiz(clefQuiz),
	CONSTRAINT question9 FOREIGN KEY(clefQuestion) REFERENCES Question(clefQuestion)
	)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE ContenuAdditionnel(
	clefCA INT NOT NULL AUTO_INCREMENT,
	clefSeance INT NOT NULL,
	nom VARCHAR(100) NOT NULL,
	urlFichier TEXT NOT NULL,
	extension VARCHAR(5) NOT NULL,
	PRIMARY KEY(clefCA),
	CONSTRAINT seance20 FOREIGN KEY(clefSeance) REFERENCES Seance(clefSeance)
	)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE Topic(
	clefTopic INT NOT NULL AUTO_INCREMENT,
	clefSeance INT NOT NULL,
	titreTopic VARCHAR(100) NOT NULL, 
	dateCreation DATE NOT NULL,
	PRIMARY KEY(clefTopic),
	CONSTRAINT seance21 FOREIGN KEY(clefSeance) REFERENCES Seance(clefSeance)
	)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE Message(
	clefMessage INT NOT NULL AUTO_INCREMENT,
	clefTopic INT NOT NULL,
	auteurMessage VARCHAR(100) NOT NULL,
	datePublication DATE NOT NULL,
	dateModif DATE,
	contenu TEXT,
	PRIMARY KEY(clefMessage),
	CONSTRAINT membre21 FOREIGN KEY(auteurMessage) REFERENCES Membre(email),
	CONSTRAINT topic22 FOREIGN KEY(clefTopic) REFERENCES Topic(clefTopic)
	)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;
